angular.module('starter.controllers', []).controller('AppCtrl', function($ionicPlatform,$rootScope,$scope, $ionicModal, $timeout) {
    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});
    // Form data for the login modal

    $ionicPlatform.ready(function() {
        if(ionic.Platform.device().platform === "iOS") {
            window.plugin.notification.local.promptForPermission();
        }
    });
    $scope.loginData = {};
    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };
    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
        localStorage.setItem("lastname", ds);
    };
    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);
        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);
    };
    //$rootScope.channels = {};
}).controller("AlarmController", function($scope, $cordovaLocalNotification,$ionicPopup) {
    $scope.programes = {};
    if (!angular.isUndefined(localStorage.fpro)) {
        $scope.programes = JSON.parse(localStorage.fpro);
    }
    $scope.likepress = function(programe) {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Bạn có muốn xoá chương trình này ?',
        });
        confirmPopup.then(function(res) {
            if (res) {//yes
                if (!angular.isUndefined(localStorage.fpro)) {
                    fpro = JSON.parse(localStorage.fpro);
                    if (!angular.isUndefined(fpro[programe.id])) {
                        delete fpro[programe.id];
                        delete $scope.programes[programe.id];
                        localStorage.fpro = JSON.stringify(fpro);
                        $cordovaLocalNotification.cancel(intval(programe.id)).then(function (result) {
                            console.log(result);
                        });
                    }
                }
            } else {//no

            }
        });
    };
}).controller('PlaylistCtrl', function($state, $scope,$rootScope,$ionicPopup,$ionicLoading) {
    $scope.err = "";
    if (angular.isUndefined($rootScope.channels)) {
        $rootScope.channels = {};
        loaddata();
    }
    function checkLabelExist(channel) {
        if (!angular.isUndefined(localStorage.channelmaps)) {
            var channelmaps = JSON.parse(localStorage.channelmaps);
            if (!angular.isUndefined(channelmaps[channel.value])) {
                return true;
            }
        }
        return false;
    }
    $scope.showPopup = function(channel) {
        $scope.data = {};

        var myPopup = $ionicPopup.show({
            template: '<input type="number" ng-model="data.label" value="'+channel.label+'">',
            title: 'Mapping Number',
            subTitle: channel.channel,
            scope: $scope,
            buttons: [{
                text: 'Cancel'
            }, {
                text: '<b>Save</b>',
                type: 'button-positive',
                onTap: function(e) {
                    if (!$scope.data.label) {
                        //don't allow the user to close unless he enters wifi password
                        e.preventDefault();
                    } else {
                        var channelmaps;
                        if (angular.isUndefined(localStorage.channelmaps)) {
                            channelmaps = {};
                        } else {
                            channelmaps = JSON.parse(localStorage.channelmaps);
                        }
                        channel.label = $scope.data.label;
                        var channelmap = {
                            "label": $scope.data.label,
                            "channel": channel,
                        };
                        channelmaps[channel.value] = channelmap;
                        localStorage.channelmaps = JSON.stringify(channelmaps);
                        return $scope.label;
                    }
                }
            }]
        });


    }
    $scope.doRefresh = function() {
        loaddata();
    }

    function loaddata() {
        var isErr = true;
        $ionicLoading.show({
            template: '<ion-spinner class="spinner-positive"></ion-spinner>'
        });
        $scope.chanels = {};
        $.ajax({
            url: 'http://vtvcab.vn/lich-phat-song',
            type: 'GET',
            dataType: 'html',
            success: function(data) {
                isErr = false;
                $('#channel option', data).each(function() {
                    var newChannel = {
                            "icon": "img/vtvcab.jpg",
                            "channel": $(this).text(),
                            "value": $(this).val(),
                        }
                    if(!angular.isUndefined(localStorage.channelmaps)){
                        var channelmaps = JSON.parse(localStorage.channelmaps);
                        if(!angular.isUndefined(channelmaps[newChannel.value])){
                            newChannel.label = channelmaps[newChannel.value].label;
                        }
                        else{
                            newChannel.label = '';
                        }
                    }
                    if(!angular.isUndefined(localStorage.fchannel)){
                        var fchannel = JSON.parse(localStorage.fchannel);
                        if(!angular.isUndefined(fchannel[newChannel.value])){
                            newChannel.isFav = true;
                        }
                        else{
                            newChannel.isFav = false;
                        }
                    }
                    $rootScope.channels[newChannel.value] = newChannel ;
                });
            },
            error: function(data, status, headers, config) {
                isErr = true;
            },
            complete: function() {
                $ionicLoading.hide();
                if(isErr == true){
                    $scope.err = "Có lỗi xảy ra, vui lòng kiểm tra kết nối internet!";
                }else{
                    $scope.err = "";
                }
                $scope.$broadcast('scroll.refreshComplete');
            }
        });
    }
    $scope.likepress = function(channel) {
            var likebtn = $('#channel-like-' + channel.value);
            if (likebtn.hasClass('active')) {
                likebtn.removeClass('active');
                if (!angular.isUndefined(localStorage.fchannel)) {
                    fchannel = JSON.parse(localStorage.fchannel);
                    if (!angular.isUndefined(fchannel[channel.value])) {
                        delete fchannel[channel.value];
                        localStorage.fchannel = JSON.stringify(fchannel);
                    }
                }
            } else {
                likebtn.addClass('active');
                var fchannel;
                if (!angular.isUndefined(localStorage.fchannel)) {
                    fchannel = JSON.parse(localStorage.fchannel);
                } else {
                    fchannel = {};
                }
                channel.isFav = true;
                fchannel[channel.value] = channel;
                localStorage.fchannel = JSON.stringify(fchannel);
            }
    }
    $scope.go = function(chanel){
        $rootScope.channelId = chanel.value;
        $rootScope.channelName = chanel.channel;
        $state.go('app.chanpro');
    }
}).controller('chanelprogrameCtrl', function($rootScope,$scope, $stateParams, $ionicLoading, ionicDatePicker, $cordovaLocalNotification) {
    var err = true;
    $scope.err = "";
    $scope.sms = "";
    var chanelId = $rootScope.channelId;
    var chanelName = $rootScope.channelName;
    console.log(chanelName);
    $scope.channel = chanelId;
    var now = new Date();
    var day;
    var month;
    var year;
    var ipObj1 = {
        callback: function(val) { //Mandatory
            now = new Date(val);
            $ionicLoading.show();
            dateupdate();
            setTimeout(checkErr, 10000);
            loaddata();
        },
        from: new Date(new Date().getFullYear(), 1, 1), //Optional
        to: new Date(new Date().getFullYear() + 1, 12, 30), //Optional
        inputDate: new Date(), //Optional
        mondayFirst: false, //Optional
        closeOnSelect: true, //Optional
        templateType: 'popup', //Optional
    };
    // function declare
    function checkErr() {
        $ionicLoading.hide();
        if (err == true) {
            $scope.err = "loaded error!";
            console.log('load err');
        }
    }

    function dateupdate() {
        day = now.getDate();
        if (day < 10) {
            day = '0' + day;
        } else {
            day = day.toString();
        }
        month = now.getMonth() + 1;
        if (month < 10) {
            month = '0' + month;
        } else {
            month = month.toString();
        }
        year = now.getFullYear().toString();
        console.log(now.getFullYear() + 1);
        $scope.date = day + '-' + month + '-' + year;
    }

    function loaddata() {
        $.ajax({
            url: 'http://www.vtvcab.vn/lich-phat-song?day=' + day + '&month=' + month + '&year=' + year + '&channel=' + chanelId,
            type: 'GET',
            dataType: 'html',
            success: function(data) {
                $ionicLoading.hide();
                err = false;
                var programes = [];
                $('.table-schedules table tbody tr', data).each(function() {
                    var col = $(this).children('td');
                    if (col.length == 1) {
                        $scope.sms = "Chưa có lịch phát sóng!";
                    } else {
                        var time = col[0].innerHTML;
                        var name = col[1].innerHTML;
                        var part = col[2].innerHTML;
                        var id = chanelId + day + time.replace(':', '');
                        var isFav = false;
                        if (!angular.isUndefined(localStorage.fpro)) {
                            var fpro = JSON.parse(localStorage.fpro);
                            if (!angular.isUndefined(fpro[id])) {
                                isFav = true;
                            }
                        }
                        var programe = {
                            'time': time,
                            'name': name.toLowerCase(),
                            'part': part,
                            'id': id,
                            'isfav': isFav,
                            'chanelName': chanelName,
                        }
                        programes.push(programe);
                    }
                });
                $scope.programes = programes;
                $scope.err = "";
            },
            error: function(data, status, headers, config) {
                err = false;
                $ionicLoading.hide();
                $scope.err = "loaded error!";
            }
        });
    }
    $scope.likepress = function(programe) {
            var likebtn = $('#like-btn-' + programe.id);
            if (likebtn.hasClass('active')) {
                likebtn.removeClass('active');
                //likebtn.css('background-image','url("../img/star.png")');
                if (!angular.isUndefined(localStorage.fpro)) {
                    fpro = JSON.parse(localStorage.fpro);
                    console.log(fpro);
                    if (!angular.isUndefined(fpro[programe.id])) {
                        delete fpro[programe.id];
                        $cordovaLocalNotification.cancel(programe.id).then(function (result) {
                            console.log(result);
                        });
                        localStorage.fpro = JSON.stringify(fpro);
                    }
                }
            } else {
                likebtn.addClass('active');
                //likebtn.css('background-image','url("../img/staractive.png")');
                var fpro;
                if (!angular.isUndefined(localStorage.fpro)) {
                    fpro = JSON.parse(localStorage.fpro);
                } else {
                    fpro = {};
                }

                programe.isfav = true;
                fpro[programe.id] = programe;
                localStorage.fpro = JSON.stringify(fpro);
                var alermTime = new Date(year + '-' + month + '-' + day + ', ' + programe.time);
                var newTime = alermTime.getTime() - (30 * 60 * 1000);
                alermTime = new Date(newTime);

                $cordovaLocalNotification.schedule({
                    id: intval(programe.id),
                    date: alermTime,
                    message: programe.name+' chiếu lúc '+programe.time+' trên kênh  ' + programe.chanelName  ,
                    title: "Chương trình yêu thích săp trình chiếu",
                    autoCancel: false,
                    sound: 'file://sound/DangerouslyAlarm.mp3'
                }).then(function() {
                    console.log("The notification has been set");
                });

            }
    }
        // processing
    dateupdate();
    loaddata();
    $ionicLoading.show({
        template: '<ion-spinner class="spinner-positive"></ion-spinner>'
    });
    setTimeout(checkErr, 10000);
    $scope.openDatePicker = function() {
        ionicDatePicker.openDatePicker(ipObj1);
    };
}).controller('ChanelMappingCtrl', function($scope,$rootScope, $ionicPopup) {
    if (!angular.isUndefined(localStorage.channelmaps)) {
        $scope.labels = JSON.parse(localStorage.channelmaps);

    } else {
        $scope.labels = {};
    }
    $scope.deleteLabel = function(label) {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Bạn có muốn xoá nhãn dán này ?',

        });
        confirmPopup.then(function(res) {
            if (res) {//yes
                var channelmaps = JSON.parse(localStorage.channelmaps);
                delete channelmaps[label.channel.value];
                $scope.labels=channelmaps;
                if(!angular.isUndefined($rootScope.channels[label.channel.value])){
                    $rootScope.channels[label.channel.value].label = '';
                }
                localStorage.channelmaps = JSON.stringify(channelmaps);
            } else {//no

            }
        });
    };
    $scope.edit = function(label) {
            $scope.data = {};
            var myPopup = $ionicPopup.show({
                template: '<input type="number" ng-model="data.newLabel" ng-value="' + label.label + '">',
                title: 'Edit Mapping Number',
                // subTitle: channel.channel,
                scope: $scope,
                buttons: [{
                    text: 'Cancel'
                }, {
                    text: '<b>Edit</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        if (!$scope.data.newLabel) {
                            //don't allow the user to close unless he enters wifi password
                            e.preventDefault();
                        } else {
                            var channelmaps = JSON.parse(localStorage.channelmaps);
                            channelmaps[label.channel.value].label = $scope.data.newLabel;
                            //console.log(channelmaps);
                            label.label = $scope.data.newLabel;

                            if(!angular.isUndefined($rootScope.channels[label.channel.value])){
                                $rootScope.channels[label.channel.value].label = $scope.data.newLabel;
                            }
                            localStorage.channelmaps = JSON.stringify(channelmaps);
                            return $scope.newLabel;
                        }
                    }
                }]
            })
        }
        // console.log($scope.labels);
}).controller('fchannelCtrl', function($scope,$rootScope,$state, $ionicPopup) {
    if (!angular.isUndefined(localStorage.fchannel)) {
        $scope.labels = JSON.parse(localStorage.fchannel);
        if (!angular.isUndefined(localStorage.channelmaps)) {
            channelmaps = JSON.parse(localStorage.channelmaps);
            $.each($scope.labels,function(k,v){
                if(!angular.isUndefined(channelmaps[k])){
                    $scope.labels[k].label = channelmaps[k].label;
                }else{
                    $scope.labels[k].label = '';
                }
            });
        }
    } else {
        $scope.labels = {};
    }
    $scope.deleteLabel = function(label) {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Bạn có muốn xoá kênh này ?',
        });
        confirmPopup.then(function(res) {
            if (res) {//yes
                var fchannel = JSON.parse(localStorage.fchannel);
                delete fchannel[label.value];
                if(!angular.isUndefined($rootScope.channels[label.value])){
                    $rootScope.channels[label.value].isFav = false;
                }
                $scope.labels=fchannel;
                localStorage.fchannel = JSON.stringify(fchannel);
            } else {//no

            }
        });
    };
    $scope.go = function(chanel){
        $rootScope.channelId = chanel.value;
        $rootScope.channelName = chanel.channel;
        $state.go('app.chanpro');
    }
        // console.log($scope.labels);
});