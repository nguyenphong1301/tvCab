// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova','ionic-datepicker']).run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
}).config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider,ionicDatePickerProvider) {
    var datePickerObj = {
      inputDate: new Date(),
      setLabel: 'Select',
      todayLabel: 'Today',
      closeLabel: 'Close',
      mondayFirst: false,
      weeksList: ["Sun", "Mon", "Tue", "Wed", "thu", "Fri", "Sat"],
      monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
      templateType: 'popup',
      from: new Date(new Date().getFullYear() , 1, 1),
      to: new Date(new Date().getFullYear()+1, 12, 30),
      showTodayButton: true,
      dateFormat: 'dd MMMM yyyy',
      closeOnSelect: false,
    };
    ionicDatePickerProvider.configDatePicker(datePickerObj);

    $stateProvider.state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })
    .state('app.playlist', {
        //cache:false,
        url: '/playlist',
        views: {
            'menuContent': {
                templateUrl: 'templates/playlist.html',
                controller: 'PlaylistCtrl'
            }
        }
    })
    .state('app.chanpro', {
         //cache:false,
        url: '/chanpro',
        views: {
            'menuContent': {
                templateUrl: 'templates/chanpro.html',
                controller: 'chanelprogrameCtrl'
            }
        }
    })
    .state('app.alarm', {
        cache:false,
        url: '/alarm',
        views: {
            'menuContent': {
                templateUrl: 'templates/alarm.html',
                controller: 'AlarmController'
            }
        }
    }).state('app.chanelmapping', {
        cache:false,
        url: '/chanelmapping',
        views: {
            'menuContent': {
                templateUrl: 'templates/chanelmapping.html',
                controller: 'ChanelMappingCtrl'
            }
        }
    }).state('app.fchanel', {
        cache:false,
        url: '/fchanel',
        views: {
            'menuContent': {
                templateUrl: 'templates/fchanel.html',
                controller: 'fchannelCtrl'
            }
        }
    });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/playlist');
});