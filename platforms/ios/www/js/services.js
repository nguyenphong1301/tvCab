var myservice = angular.module('starter.services', ['ngStorage']);
myservice.factory('alerm', function($cordovaLocalNotification) {
    return {
        setAlerm: function() {
            var alermlist = {};
            if (!angular.isUndefined(localStorage.fpro)) {
                angular.forEach(alermlist, function(value, index) {
                    var valueTime = value.time.split(":");
                    var alarmTime = new Date();
                    alarmTime.setHours(valueTime[0], valueTime[1]);
                    alarmTime.setMinutes(alarmTime.getMinutes() - 30);
                    $cordovaLocalNotification.add({
                        id: "1234",
                        date: alarmTime,
                        message: "value.program",
                        title: "value.time",
                        autoCancel: true,
                        sound: 'file://sound/DangerouslyAlarm.mp3'
                    }).then(function() {
                        console.log("The notification has been set");
                    });
                });
            }
        }
    }
});